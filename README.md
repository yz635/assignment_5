### Hello, this is yz635 trying to import terms.
Do [4+ hours, 2 points]
Create a GitLab repo named 'assignment5' for this week's work. Add me as a 'Reporter'

Draw UML class diagrams for Ledger's xact and account classes. Submit them as .png or .jpg files in the repo. You may use any diagramming tool you find convenient, ranging from pen and paper to Draw.io, LucidChart, OmniGraffle, Visio. I'd suggest Draw.io. Post comments on Slack. As always, you are welcome to collaborate; note who your collaborator(s) are.

Draw a sequence diagram for the sequence of events caused by calling journal_t::add_account

How does Ledger represent money, e.g. US Dollars and Swiss Francs?

Complete the coding (and process) exercises below.

Coding [5+ hours, 2 points]
Following the PSP, TDD, and refactoring practices discussed and practiced so far in class...

Copy the code for the TermFreq application you wrote in assignment 2 and the KWIC application you wrote in assignment 3 in to the assignment 5 repo. Copy Defect.md and any other issue templates you may have created as well.
Develop a jUnit test suite for the requirements of the TermFreq application.
Develop a jUnit test suite for the requirements of the KWIC application.
Create a Java package, edu.duke.fintech512.terms to contain all code common to both the TermFreq and KWIC applications. Refactor the applications to contain as little code as possible, and for the common code to contain as much code as possible (without duplication.)
NOTE: Feel free to approach these tasks in the order that makes sense to you; follow the usual practices of PSP and frequent git commits.
