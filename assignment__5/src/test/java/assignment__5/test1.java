package assignment__5;

import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class test1 {

	@Test
	public void test_readin() {
		term_count t = new term_count();
		String answer = "\"White tigers live mostly in India Wild lions live mostly in Africa\"" + " ";
		String expected = t.readin();
		Assert.assertEquals(answer, expected);
	}

	@Test
	public void test_add_stop_word() {
		term_count t = new term_count();
		Set<String> expected = t.add_stop_word();
		int num = expected.size();
		int want = 10;
		Assert.assertEquals(num, want);
	}

	@Test
	public void test_count() {
		term_count t = new term_count();
		t.readin();
		t.add_stop_word();
		Map<String, Integer> expected = t.count();
		int num = expected.size();
		int want = 8;
		Assert.assertEquals(num, want);

	}

	@Test
	public void test_sort() {
		term_count t = new term_count();
		t.readin();
		t.add_stop_word();
		t.count();
		t.sort();
		String answer = "live";
		Integer value = 2;
		Assert.assertEquals(answer, t.sort().get(0).getKey());
		Assert.assertEquals(value, t.sort().get(0).getValue());
	}

}
