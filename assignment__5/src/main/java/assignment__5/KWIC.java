package assignment__5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class KWIC {
	public Set<String> stop;
	public List<String> keyword;
	public List<String> title;

	KWIC() {
		stop = new HashSet<String>();
		keyword = new ArrayList<>();
		title = new ArrayList<>();
	}

	public void readin() {
		Scanner scan = new Scanner(System.in);
		while (true) {
			String s = scan.nextLine();
			if (!s.equals("::")) {
				stop.add(s.toLowerCase());
			} else {
				break;
			}
		}
		while (scan.hasNext()) {
			String line = scan.nextLine();
			title.add(line.toLowerCase());
		}
	}

	public void find_keyword() {
		for (int i = 0; i < title.size(); i++) {
			String[] arr = title.get(i).split(" ");

			for (int j = 0; j < arr.length; j++) {
				if (stop.contains(arr[j]) || keyword.contains(arr[j])) {

				} else {
					keyword.add(arr[j]);
				}
			}
		}
	}

	public void print_result() {
		Collections.sort(keyword, new StringComparator());

		int flag2 = 0;
		for (int j = 0; j < keyword.size(); j++) {
			for (int i = 0; i < title.size(); i++) {
				String str = title.get(i);
				flag2 = 0;
				while (str.contains(keyword.get(j))) {
					int start = str.indexOf(keyword.get(j));
					int end = start + keyword.get(j).length();
					System.out.println(title.get(i).substring(0, start + flag2)
							+ title.get(i).substring(start + flag2, flag2 + end).toUpperCase()
							+ title.get(i).substring(end + flag2));
					str = str.substring(start + keyword.get(j).length());
					flag2 = start + keyword.get(j).length();
				}
			}
		}

	}

	static class StringComparator implements Comparator<Object> {
		@Override
		public int compare(Object object1, Object object2) {
			return ((String) object1).compareTo((String) object2);
		}
	}

	public static void main(String[] args) {
		KWIC k = new KWIC();
		k.readin();
		k.find_keyword();
		k.print_result();
	}

}
