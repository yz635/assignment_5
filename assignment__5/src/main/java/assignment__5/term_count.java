package assignment__5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class term_count {
	private String result = "";
	private Set<String> hs = new HashSet<String>();
	private Map<String, Integer> map = new HashMap<>();
	private List<Map.Entry<String, Integer>> list;

	public String readin() {
		File file = new File("lion.txt");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String s = null;
			while ((s = br.readLine()) != null) {
				if (s.length() == 0)
					continue;
				else
					result = result + s + " ";
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Set<String> add_stop_word() {
		hs.add("in");
		hs.add("the");
		hs.add("a");
		hs.add("for");
		hs.add("and");
		hs.add("at");
		hs.add("to");
		hs.add("with");
		hs.add("of");
		hs.add("on");
		return hs;
	}

	public Map<String, Integer> count() {
		String[] arr = result.split(" ");
		for (String str : arr) {
			str = str.replaceAll("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&;*（）——+|{}【】‘；：’。，、？|-]", "");
			str = str.replaceAll("\"", "");
			str = str.replaceAll("”", "");
			str = str.replaceAll("“", "");

			String n = str.toLowerCase();
			if (hs.contains(n)) {

			} else {
				Integer num = map.get(n);
				map.put(n, num == null ? 1 : num + 1);
			}

		}
		return map;
	}

	public List<Entry<String, Integer>> sort() {
		list = new ArrayList<Map.Entry<String, Integer>>(map.entrySet()); // 转换为list

		list.sort(new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		list.sort(new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});
		return list;

	}

	public void print() {
		for (int i = 0; i < 25 && i < list.size(); i++) {
			System.out.println(list.get(i).getKey() + " - " + list.get(i).getValue());
		}
	}

	public static void main(String[] args) {
		term_count t = new term_count();
		t.readin();
		t.add_stop_word();
		t.count();
		t.sort();
		t.print();
	}
}
