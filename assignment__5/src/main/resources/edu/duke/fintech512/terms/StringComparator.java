package edu.duke.fintech512.terms;

import java.util.Comparator;

public class StringComparator implements Comparator<Object> {
	@Override
	public int compare(Object object1, Object object2) {
		return ((String) object1).compareTo((String) object2);
	}

}
