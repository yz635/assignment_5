package update_code;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.duke.fintech512.terms.Common;

public class Term_fre_update {
	private String result = "";
	private Set<String> hs = new HashSet<String>();
	private Map<String, Integer> map = new HashMap<>();
	private List<Map.Entry<String, Integer>> list;
	public List<String> in = new ArrayList<>();

	public String parse() {
		Common c = new Common();
		in = c.readin("lion.txt");

		for (int i = 0; i < in.size(); i++) {
			result = result + in.get(i) + " ";
		}
		return result;
	}

	public Set<String> add_stop_word() {
		hs.add("in");
		hs.add("the");
		hs.add("a");
		hs.add("for");
		hs.add("and");
		hs.add("at");
		hs.add("to");
		hs.add("with");
		hs.add("of");
		hs.add("on");
		return hs;
	}

	public Map<String, Integer> count() {
		String[] arr = result.split(" ");
		for (String str : arr) {
			str = str.replaceAll("\"", "");
			String n = str.toLowerCase();
			if (hs.contains(n)) {

			} else {
				Integer num = map.get(n);
				map.put(n, num == null ? 1 : num + 1);
			}

		}
		return map;
	}

	public List<Entry<String, Integer>> sort() {
		list = new ArrayList<Map.Entry<String, Integer>>(map.entrySet()); // 转换为list

		list.sort(new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		list.sort(new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});
		return list;

	}

	public void print() {
		for (int i = 0; i < 25 && i < list.size(); i++) {
			System.out.println(list.get(i).getKey() + " - " + list.get(i).getValue());
		}
	}

	public static void main(String[] args) {
		Term_fre_update t = new Term_fre_update();
		t.parse();
		t.add_stop_word();
		t.count();
		t.sort();
		t.print();
	}
}
