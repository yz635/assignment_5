package update_code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.duke.fintech512.terms.Common;
import edu.duke.fintech512.terms.StringComparator;

public class KWIC_update {
	public List<String> in;
	public Set<String> stop;
	public List<String> keyword;
	public List<String> title;

	KWIC_update() {
		stop = new HashSet<String>();
		keyword = new ArrayList<>();
		title = new ArrayList<>();
	}

	public void parse() {
		Common c = new Common();
		in = c.readin("System.in");
		int flag = 0;
		for (int i = 0; i < in.size(); i++) {
			if (in.get(i).equals("::")) {
				flag = 1;
				continue;
			}
			if (flag == 0) {
				stop.add(in.get(i).toLowerCase());
			}
			if (flag == 1) {
				title.add(in.get(i).toLowerCase());
			}

		}
	}

	public void find_keyword() {
		for (int i = 0; i < title.size(); i++) {
			String[] arr = title.get(i).split(" ");

			for (int j = 0; j < arr.length; j++) {
				if (stop.contains(arr[j]) || keyword.contains(arr[j])) {

				} else {
					keyword.add(arr[j]);
				}
			}
		}
	}

	public void print_result() {
		StringComparator com = new StringComparator();
		Collections.sort(keyword, com);
		int flag2 = 0;
		for (int j = 0; j < keyword.size(); j++) {
			for (int i = 0; i < title.size(); i++) {
				String str = title.get(i);
				flag2 = 0;
				while (str.contains(keyword.get(j))) {
					int start = str.indexOf(keyword.get(j));
					int end = start + keyword.get(j).length();
					System.out.println(title.get(i).substring(0, start + flag2)
							+ title.get(i).substring(start + flag2, flag2 + end).toUpperCase()
							+ title.get(i).substring(end + flag2));
					str = str.substring(start + keyword.get(j).length());
					flag2 = start + keyword.get(j).length();
				}
			}
		}

	}

	public static void main(String[] args) {
		KWIC_update k = new KWIC_update();
		k.parse();
		k.find_keyword();
		k.print_result();
	}

}
